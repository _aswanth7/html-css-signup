let signUpBtn = document.querySelector("#signUp");
let errorMsg = document.querySelector("#messageTag");

const onSignup = (e) => {
  e.preventDefault();
  let name = document.querySelector("#name").value;
  let email = document.querySelector("#email").value;
  let password = document.querySelector("#password").value;
  let cpassword = document.querySelector("#c-password").value;
  let check = document.querySelector("#check").checked;
  let mailReg = /^.+@.+\..+$/;
  let passwordReg = /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).*$/;
  let message = "";
  let error = true;
  errorMsg.setAttribute("class", "");
  if (
    name == "" ||
    email == "" ||
    password == "" ||
    cpassword == "" ||
    !check
  ) {
    message = "All fields are required!";
  } else {
    if (name.length < 5) {
      message = "length of name should be more than 5 character!";
    } else if (!mailReg.test(email)) {
      message = "Enter a valid Email!";
    } else if (!passwordReg.test(password)) {
      message = "Password should contain Alphabet Number Special Character!";
    } else if (password.length < 6) {
      message = "length of password should be more than 5 character!";
    } else if (password != cpassword) {
      message = "Password should Match!";
    } else {
      message = "Sign Up Successful";
      error = false;
    }
  }
  errorMsg.style.display = "block";
  errorMsg.innerText = message;
  if (error) {
    errorMsg.classList.add("message-error");
  } else {
    errorMsg.classList.add("message-success");
  }
};

signUpBtn.addEventListener("click", onSignup);
